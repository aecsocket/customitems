# CustomItems
A Spigot plugin to add your own items with metadata.
## For developers
You will need to create your own ItemManager and create your own classes that extend Item. You can use the ItemManager's methods to register an Item, and save/load to/from the CustomItem plugin's config file.
## Download
### Compilation
Download the repo/clone it, add the Spigot API JAR file as a dependency and build (e.g. Build Artifact in IntelliJ).
### Download
Currently, there are no prebuilt JARs for download.