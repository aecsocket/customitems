package me.aecsocket.customitems;

import org.bukkit.plugin.java.JavaPlugin;

/** The main plugin class. */
public class CustomItems extends JavaPlugin {
    private static CustomItems instance;

    public static CustomItems getInstance() { return instance; }

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
    }
}
