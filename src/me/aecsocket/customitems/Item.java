package me.aecsocket.customitems;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/** A custom item.
 * <br>
 * NOTE: It is recommended to create a static method which converts an ItemStack into your class that extends Item. */
public interface Item extends ConfigurationSerializable {
    /** Gets the UUID of the instance.
     * @return The UUID. */
    UUID getUniqueId();

    /** Gets the final item
     * @return The item. */
    ItemStack getItem();
}
