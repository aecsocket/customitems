package me.aecsocket.customitems;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/** The class that controls all Items.
 * <br>
 * Create one for your own plugin. */
public class ItemManager {
    private String id;
    private Map<UUID, Item> items;

    public ItemManager(String id, Map<UUID, Item> items) {
        this.id = id;
        this.items = items;
    }

    public ItemManager(String id) {
        this(id, new HashMap<>());
    }

    /** Gets the identifier that is used in saving to file.
     * @return The identifier. */
    public String getId()                       { return id; }

    /** Gets a Map of all of the items.
     * @return The Map. */
    public Map<UUID, Item> getItems()           { return new HashMap<>(items); }

    /** Sets the Map of all items.
     * @param items The new Map. */
    public void setItems(Map<UUID, Item> items) { this.items = items; }

    /** Adds or updates an entry in the manager.
     * @param item The item to add/update. */
    public void updateItem(Item item)   { items.put(item.getUniqueId(), item); }

    /** Removes an entry in the manager, if it exists.
     * @param uuid The UUID of the item to remove. */
    public void removeItem(UUID uuid)   { items.remove(uuid); }

    /** Returns whether or not the manager has a UUID registered.
     * @param uuid The UUID registered.
     * @return If it is registered or not. */
    public boolean hasItem(UUID uuid)   { return items.containsKey(uuid); }

    /** Gets the item from a UUID.
     * @param uuid The UUID.
     * @return The item. */
    public Item getItem(UUID uuid)      { return items.getOrDefault(uuid, null);}

    /** Saves a specific item to file, under the identifier.
     * @param item The item. */
    public void save(Item item) {
        FileConfiguration config = CustomItems.getInstance().getConfig();
        config.set(String.format("%s.%s", id, item.getUniqueId()), item);
    }

    /** Saves all the items currently in memory in this instance to file. */
    public void saveAll() {
        for (Item item : items.values())
            save(item);
    }

    /** Gets an item that is saved on file under this identifier.
     * @param uuid The UUID of the item.
     * @return The item. */
    public Item load(UUID uuid) {
        FileConfiguration config = CustomItems.getInstance().getConfig();
        String path = String.format("%s.%s", id, uuid);
        return (Item)config.get(path);
    }
}
